package com.example.demo.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
    @Autowired
    private AmqpTemplate amqpTemplate;

   public void sendMsg(String key,String msg){
        amqpTemplate.convertAndSend(key,msg);
    }
}
