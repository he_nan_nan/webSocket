package com.example.demo.model;

/**
 * @Title UserInfo
 * @Description
 * @Author henannan
 * @Createdate 2019/12/13 15:29
 * @Updatedate 2019/12/13 15:29
 * @Version v1.0
 * @Copyright
 */
public class UserInfo {
	private String vip;

	public String getVip() {
		return vip;
	}

	public void setVip(String vip) {
		this.vip = vip;
	}
}
