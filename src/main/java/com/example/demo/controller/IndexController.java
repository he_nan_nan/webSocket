package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import com.example.demo.model.RestResp;
import com.example.demo.model.UserInfo;
import com.example.demo.utils.CookieUtils;
import com.example.demo.utils.SensitiveWordUtil;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@CrossOrigin
public class IndexController {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private AmqpTemplate amqpTemplate;

    @RequestMapping("/page1")
    public String toPage1(){
        return "demo1";
    }
    @RequestMapping("/page2")
    public String toPage2(){
        return "demo2";
    }

    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, 60);

        System.out.println(c.getTime());
    }

    /**
     *
     * @param msg 消息
     * @param id 弹幕房间id
     * @param type 弹幕类型
     * @return
     */
    @RequestMapping("addMsg")
    @ResponseBody
    public RestResp addMsg(@RequestParam("msg")String msg, @RequestParam("id")String id, @RequestParam("type")String type,/*@RequestParam("token")String token,*/ HttpServletRequest httpServletRequest){
        RestResp restResp = new RestResp();
        Cookie[] cookies = httpServletRequest.getCookies();
       /* if (StringUtils.isEmpty(token)){
            restResp.setCode("1");
            restResp.setMsg("请先登录");
            return restResp;
        }*/
        //根据cookieName获取用户的token
        //String token = CookieUtils.getCookieValue(httpServletRequest, "cookieName",true);
        //如果token为空 不允许发送弹幕
        /*if (StringUtils.isEmpty(token)){
            restResp.setCode("1");
            restResp.setMsg("请先登录");
        }else {*/
        //todo 去redis获取用户信息，判断用户类型
       /* String userMsg = (String)redisTemplate.opsForValue().get("");
        if (StringUtils.isEmpty(userMsg)){
            restResp.setCode("1");
            restResp.setMsg("请先登录");
            return restResp;
        }
*/
        try {
            //UserInfo userInfo = JSON.parseObject(userMsg, UserInfo.class);
            //如果是vip,直接放入mq推送
            if (true/*userInfo.getVip().equals('1')*/){
                HashMap<Object, Object> hashMap1 = new HashMap<>();
                hashMap1.put("id",id);
				msg = SensitiveWordUtil.replaceSensitiveWord(msg, '*', SensitiveWordUtil.MinMatchTYpe);
                hashMap1.put("msg",msg);
                amqpTemplate.convertAndSend("item",hashMap1);
                restResp.setCode("0");
                restResp.setMsg("success");
            }else {
                //todo 存入数据库或者redis
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return restResp;
    }
}
