package com.example.demo.controller;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;


@ServerEndpoint(value = "/WebSocket")
@Component
public class WebSocketServer {
    private String id;

    public static int onlineCount = 0;

    //public static CopyOnWriteArraySet<WebSocketServer> wbSocketMap = new CopyOnWriteArraySet<>();

	public static ConcurrentHashMap<String,CopyOnWriteArraySet<WebSocketServer>> wbSocketMap = new ConcurrentHashMap<>();

	private Session session;
    @OnMessage
    public void onMessage(String message,Session session)throws IOException{

        System.out.println("来自客户端的消息："+message);
    }
    @OnOpen
    public void onOpen(Session session)throws Exception{
        String queryString = session.getQueryString();
		this.session=session;
        this.id=queryString;
		CopyOnWriteArraySet copyOnWriteArraySet = wbSocketMap.get(id);
		if (copyOnWriteArraySet!=null){
			copyOnWriteArraySet.add(this);
		}else {
			synchronized(WebSocketServer.class){
				if (copyOnWriteArraySet==null){
					copyOnWriteArraySet = new CopyOnWriteArraySet();
				}
			}
			wbSocketMap.put(id,copyOnWriteArraySet);
			copyOnWriteArraySet.add(this);
		}

        addOnlineCount(queryString);
		System.out.println(WebSocketServer.onlineCount);

    }
    @OnClose
    public void onClose(){
        wbSocketMap.remove(this);
        subOnlineCount();

    }
    /**
     * 发生错误时调用
     */
    @OnError
    public void onError(Session session, Throwable error) {
        System.out.println("发生错误");

        error.printStackTrace();
    }
    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
        //this.session.getAsyncRemote().sendText(message);
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount(String queryString) {

        WebSocketServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketServer.onlineCount--;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
