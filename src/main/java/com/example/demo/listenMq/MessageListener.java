package com.example.demo.listenMq;

import com.example.demo.controller.WebSocketServer;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 弹幕信息监听
 */
@Component
public class MessageListener {

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "item.queue",durable = "true"),
            exchange = @Exchange(value = "demo.item.exchange",ignoreDeclarationExceptions = "true",
            type = ExchangeTypes.DIRECT
            ),
            key = "item"
    ))
    public void getMessageAndSend(Map<String,String> message){
    	//去除房间号
        String id = message.get("id");
        //根据房间号获取当前房间所有的连接
		CopyOnWriteArraySet copyOnWriteArraySet = WebSocketServer.wbSocketMap.get(id);
		if (copyOnWriteArraySet!=null){
			//遍历连接，分别发送信息
			for (Object o : copyOnWriteArraySet) {
				WebSocketServer webSocketServer = (WebSocketServer) o;
				try {
					webSocketServer.sendMessage(message.get("msg"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		/*for (WebSocketServer webSocketServer : WebSocketServer.wbSocketMap) {
            try {
                if (id.equals(webSocketServer.getId())){
                    webSocketServer.sendMessage(message.get("msg"));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }
}
